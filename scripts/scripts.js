/**
 * Created by Алекс on 29.10.2016.
 */

document.addEventListener('DOMContentLoaded', function () {
    document.getElementById('getSiteForm').addEventListener('submit', function (e) {
        var formData = new FormData(e.target),
            errorDiv = document.getElementById('errors'),
            xmlhttp = new XMLHttpRequest();

        e.preventDefault();

        errorDiv.innerHTML = '';

        if (!formData.get('name')) {
            var nameError = document.createElement('li');
            nameError.innerHTML = 'Имя не указано';
            errorDiv.appendChild(nameError);
        }

        if (!formData.get('contact')) {
            var contactError = document.createElement('li');
            contactError.innerHTML = 'Контакты не указаны';
            errorDiv.appendChild(contactError);
        }

        if (errorDiv.children.length) {
            return;
        }

        xmlhttp.open('POST', '/feedback.php', true);
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4) {
                if(xmlhttp.status == 200) {
                    e.target.innerHTML = 'Спасибо за отзыв!';
                }
            }
        };
        xmlhttp.send(formData);
    })
});