<?php
    $str = file_get_contents('users.json');
    $users = json_decode($str, true);
    $login = $_GET['username'];
    if (!$login) {
        $login = "default";
    }
    $user = $users[$login];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?= $user['name'] ?> - World</title>
    <link rel="stylesheet" href="/styles/styles.css">
    <link rel="stylesheet" href="/styles/main.css">
    <link rel="stylesheet" href="/styles/responsive.css">
    <link rel="stylesheet" href="/styles/font-awesome.min.css">

    <script language="JavaScript" src="/scripts/scripts.js"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
</head>
<body>
<div class="wrapper header">
    <header>
        <a href="https://ya.ru"><img class="logo" src="/images/logo.png" alt=""></a>
        <div class="contacts">
            <a href="whatsapp://send?text=<?= urlencode('HELO WOR(L)D'); ?>&abid=<?= $user['whatsapp'] ?>" data-action="share/whatsapp/share" class="contact whatsapp"><i class="fa fa-whatsapp" aria-hidden="true"></i><?= $user['whatsapp'] ?></a>
            <a href="skype:<?= $user['skype'] ?>" class="contact skype"><i class="fa fa-skype" aria-hidden="true"></i><?= $user['skype'] ?></a>
        </div>
        <nav>
            <ul>
                <li>О компании</li>
                <li>Helo</li>
                <li>Отзывы</li>
                <li>Как начать</li>
            </ul>
        </nav>
    </header>
</div>
<div class="wrapper-fluid more-block">
    <div class="wrapper">
        <div class="description">
            <ul>
                <li>Как заработать <strong>129,140 рублей</strong> за 7 дней с продуктом, о котором не знают все ваши
                    друзья?
                </li>
                <li>Как за <strong>8 месяцев</strong> купить квартиру за наличку?</li>
                <li>Как <strong>уволиться с работы</strong> и жить в своё удовольствие, зарабатывая на технологиях?</li>
            </ul>
            <a href="<?= $user['world_login'] ?>" class="button">Узнать подробности</a>
        </div>
        <div class="video">
            <iframe height="180" src="https://www.youtube.com/embed/jGi53dNZl5c?rel=0" frameborder="0"
                    allowfullscreen></iframe>
        </div>
    </div>
</div>
<div class="wrapper-fluid features-block">
    <div class="wrapper">
        <ul>
            <li>
                <div class="icon"><i class="fa fa-check" aria-hidden="true"></i></div>
                <span>5 лет на рынке</span>
            </li>
            <li>
                <div class="icon"><i class="fa fa-check" aria-hidden="true"></i></div>
                <span>узнаваемый бренд</span>
            </li>
            <li>
                <div class="icon"><i class="fa fa-check" aria-hidden="true"></i></div>
                <span>высокое качество</span>
            </li>
            <li>
                <div class="icon"><i class="fa fa-check" aria-hidden="true"></i></div>
                <span>выгодное партнёрство</span>
            </li>
        </ul>
    </div>
</div>
<div class="wrapper company-description">
    <div class="border">
        <p>
            <strong>Wor(l)d Global Network</strong> — международная корпорация, основанная в 2011 году в Италии.
            На сегодняшний день <strong>Wor(l)d</strong> развивается в более чем 125 странах мира.
            <strong>Wor(l)d</strong> является признанным лидером МЛМ компаний по международному развитию на рынке
            мобильных и носимых технологий. Сочетание мобильные технологии и прямые продажи позволило сотням тысяч людей
            во всём мире стать богатыми, обеспеченными людьми.
            В 2016 году <strong>Wor(l)d</strong> начал своё движение на новом рынке носимых технологий. Это ещё больше
            интерес у людей, это
            ещё быстрее динамика роста, это уже фантастические товарообороты и как следствие, еще больше богатых людей
            во
            всём мире, которые полностью улучшили качество своей жизни вместе с <strong>Wor(l)d</strong>. Сегодня с этой
            возможностью
            знакомитесь и Вы.
        </p>
        <div class="video">
            <iframe width="320" height="204" src="https://www.youtube.com/embed/CbOeV2JuefI?rel=0" frameborder="0"
                    allowfullscreen></iframe>
        </div>
    </div>
</div>
<div class="wrapper facts">
    <h1>Факты надёжности Wor(l)d</h1>
    <ul>
        <li>
            <img src="/images/facts/fact1.png" alt="">
            <h2>Wor(l)d исправно платит налоги</h2>
            <a href="https://egrul.nalog.ru/" class="button">Подробнее</a>
        </li>
        <li>
            <img src="/images/facts/fact2.png" alt="">
            <h2>Wor(l)d котируется на бирже NasDaq</h2>
            <a href="http://www.nasdaq.com/en/symbol/wrmt" class="button">Подробнее</a>
        </li>
        <li>
            <img src="/images/facts/fact3.png" alt="">
            <h2>Wor(l)d занимает 77 место в ТОП-100</h2>
            <a href="https://www.businessforhome.org/2016/01/100-solid-top-mlm-companies-for-2016/" class="button">Подробнее</a>
        </li>
        <li>
            <img src="/images/facts/fact4.png" alt="">
            <h2>9 человек из Wor(l)d в ТОП-200 </h2>
            <a href="https://www.businessforhome.org/2016/02/top-200-worldwide-earners-in-mlm-february-2016/"
               class="button">Подробнее</a>
        </li>
        <li>
            <img src="/images/facts/fact5.png" alt="">
            <h2>Президент на обложке Networking Times</h2>
            <a href="http://www.networkingtimes.com/nt/15/04/3740.phtml" class="button">Подробнее</a>
        </li>
        <li>
            <img src="/images/facts/fact6.png" alt="">
            <h2>Wor(l)d имеет обширную географию</h2>
            <a href="https://www.worldgn.com/company/offices/" class="button">Подробнее</a>
        </li>
    </ul>
</div>
<div class="wrapper enroll enroll0">
    <a href="<?= $user['world_login'] ?>" class="button">Стать партнёром Wor(l)d</a>
</div>
<div class="wrapper-fluid introduction">
    <div class="wrapper">
        <div class="text">
            <h2>знакомьтесь,<br>ВАШ ОРАКУЛ ЗДОРОВЬЯ<br>И ОБРАЗА ЖИЗНИ!</h2>
            <img src="/images/helo-white.png" alt="">
        </div>
        <div class="video">
            <iframe width="380" height="204" src="https://www.youtube.com/embed/5LJaSBBBMCI?rel=0" frameborder="0"
                    allowfullscreen></iframe>
        </div>
    </div>
</div>
<div class="wrapper parameters">
    <h1>HELO ВЫВОДИТ НА ЭКРАН вашего смаРТФОНА ВАЖНЫЕ ДЛЯ ЖИзНИ ПАРАМЕТРЫ</h1>
    <h2>Мониторинг здоровья в реальном времени и предсказание болезни теперь в пределах досягаемости. Helo — это
        браслет, который можно носить 24/7. Он отслеживает Ваши жизненные показатели в реальном времени и предоставляет
        данные, при помощи которых Вы будете осведомлены о состоянии своего здоровья, образа жизни, сможете улучшить
        свое здоровье и физическое самочувствие.</h2>
    <ul>
        <li>
            <div class="icon"><img src="/images/icons/icon1.png" alt=""></div>
            <div class="text">
                <h2>ИЗМЕРЯЕТ КРОВЯНОЕ ДАВЛЕНИЕ</h2>
                <p>HELO регулярно измеряет кровяное давления в течение 24 часов, 7 дней в неделю.</p>
            </div>
        </li>
        <li>
            <div class="icon"><img src="/images/icons/icon2.png" alt=""></div>
            <div class="text">
                <h2>ПОКАЗЫВАЕТ ЭЛЕКТРОКАРДИОГРАММУ</h2>
                <p>Вы видели как стучит ваше сердце? HELO показывает точное ЭКГ и даёт базовые расшифровки. </p>
            </div>
        </li>
        <li>
            <div class="icon"><img src="/images/icons/icon3.png" alt=""></div>
            <div class="text">
                <h2>ИЗМЕРЯЕТ ЧАСТОТУ ДЫХАНИЯ И ПУЛЬС</h2>
                <p>HELO измеряет дыхательный ритм — количество вдохов в течение определенного периода времени.</p>
            </div>
        </li>
        <li>
            <div class="icon"><img src="/images/icons/icon4.png" alt=""></div>
            <div class="text">
                <h2>ИЗМЕРЯЕТ ВСЕ ФИТНЕС-ПАРАМЕТРЫ</h2>
                <p>Теперь Вы всегда знаете количество пройденных шагов, дистанцию и количество сожженых каллорий. </p>
            </div>
        </li>
        <li>
            <div class="icon"><img src="/images/icons/icon5.png" alt=""></div>
            <div class="text">
                <h2>ЗДОРОВЬЕ РОДНЫХ И БЛИЗКИХ ЛЮДЕй</h2>
                <p>Теперь Вы на любом расстоянии видите на своем смартфоне параметры ваших родных и близких. </p>
            </div>
        </li>
        <li>
            <div class="icon"><img src="/images/icons/icon6.png" alt=""></div>
            <div class="text">
                <h2>ПОКАЗЫВАЕТ НАСТРОЕНИЕ И УСТАЛОСТЬ</h2>
                <p>Измеряя вариабельность сердечного ритма, HELO определяет психоэмоциональное состояние организма.</p>
            </div>
        </li>
        <li>
            <div class="icon"><img src="/images/icons/icon7.png" alt=""></div>
            <div class="text">
                <h2>НОРМАЛИЗУЕТ ДАВЛЕНИЕ, САХАР В КРОВИ</h2>
                <p>В браслете присутствуют камни Германия, Гематита, Гималайской соли, они нормализуют эти
                    параметры.</p>
            </div>
        </li>
        <li>
            <div class="icon"><img src="/images/icons/icon8.png" alt=""></div>
            <div class="text">
                <h2>НОРМАЛИЗУЕТ СОН И ОБЩЕЕ СОСТОЯНИЕ</h2>
                <p>Первое что Вы ощущаете, это нормализация сна. Вы начинаете спать как в детстве и утром полны сил.</p>
            </div>
        </li>
    </ul>
</div>
<div class="wrapper-fluid samples">
    <div class="wrapper">
        <h1>Примеры использования БРАСЛЕТА HELO</h1>
        <h2>ПОЛЕЗНЫЕ ВИДЕО, КОТОРЫЕ СТОИТ ПОСМОТРЕТЬ</h2>
        <ul>
            <li>
                <iframe height="200" src="https://www.youtube.com/embed/AOmbrzOqjCU?rel=0" frameborder="0"
                        allowfullscreen></iframe>
            </li>
            <li>
                <iframe height="200" src="https://www.youtube.com/embed/N-NSqknVQGU?rel=0" frameborder="0"
                        allowfullscreen></iframe>
            </li>
            <li>
                <iframe height="200" src="https://www.youtube.com/embed/wuBuPVjXXJk?rel=0" frameborder="0"
                        allowfullscreen></iframe>
            </li>
            <li>
                <iframe height="200" src="https://www.youtube.com/embed/a1l5A-xQ56o?rel=0" frameborder="0"
                        allowfullscreen></iframe>
            </li>
            <li>
                <iframe height="200" src="https://www.youtube.com/embed/zaScG42KICg?rel=0" frameborder="0"
                        allowfullscreen></iframe>
            </li>
            <li>
                <iframe height="200" src="https://www.youtube.com/embed/tUX4C7w6Cg0?rel=0" frameborder="0"
                        allowfullscreen></iframe>
            </li>
        </ul>
        <a href="<?= $user['world_login'] ?>" class="button">Купить браслет HELO</a>
    </div>
</div>
<div class="wrapper helo-features">
    <h1>Отзывы ДОВОЛЬНЫХ КЛИЕНТОВ И ПАРТНёРОВ</h1>
    <h2>Людям нравится использовать HELO и зарабатывать на рекомендациях</h2>
    <ul>
        <li>
            <div class="icon"><i class="fa fa-heart" aria-hidden="true"></i></div>
            <h2>своЁ здоровье</h2>
            <ul>
                <li>Измеряет кровяное давление;</li>
                <li>Делает ЭКГ (Электрокардиограмму);</li>
                <li>Измеряет сердечный ритм;</li>
                <li>Измеряет частоту дыхания;</li>
                <li>Измеряет кислород в крови;</li>
                <li>Сканирует настроение/усталость;</li>
                <li>Делает проверку сердца;</li>
                <li>Измеряет шаги, дистанцию и калории;</li>
                <li>Сканирует качество сна.</li>
            </ul>
        </li>
        <li>
            <div class="icon"><i class="fa fa-heart" aria-hidden="true"></i></div>
            <h2>ЗДОРОВЬЕ БЛИЗКИХ</h2>
            <ul>
                <li>Мониторит параметры жизни родных;</li>
                <li>Оповещает при нажатии кнопки SOS;</li>
                <li>Уведомляет о местоположении любимых;</li>
                <li>Оповещает об изменении жизненных параметров родных;</li>
                <li>Прогнозирует тяжелые заболевания;</li>
                <li>Даёт медицинские подсказки и советы;</li>
                <li>Даёт душевное спокойствие за самых близких, а это бесценно.</li>
            </ul>
        </li>
        <li>
            <div class="icon"><i class="fa fa-heart" aria-hidden="true"></i></div>
            <h2>КОРРЕКЦИЯ ЗДОРОВЬЯ</h2>
            <ul>
                <li>Улучшает качество сна;</li>
                <li>Нормализует кровяное давление;</li>
                <li>Нормализует сахар в крови;</li>
                <li>Повышает кровообращение;</li>
                <li>Улучшает качество крови;</li>
                <li>Выводит шлаки и токсины;</li>
                <li>Способствует здоровому похудению;</li>
                <li>Оказывает огромное положительное влияние на организм путем Германия, Гематита и Гималайской соли.
                </li>
            </ul>
        </li>
    </ul>
</div>
<div class="wrapper feedback">
    <h1>Отзывы ДОВОЛЬНЫХ КЛИЕНТОВ И ПАРТНёРОВ</h1>
    <h2>Людям нравится использовать HELO и зарабатывать на рекомендациях</h2>
    <ul>
        <li class="item">
            <div class="avatar">
                <img src="/images/user1.png" alt="" class="avatar">
            </div>
            <h1>Евгений Лапин. Предприниматель.</h1>
            <h2>Чехов</h2>
            <p>
                Здравствуйте. Обязательно обратитите свое внимание на Helo "Диагностика и Контроль здоровья в режиме
                реального времени". Благодаря HELO я узнал о том, что у меня бывают резкие скачки давления во сне,
                около 4-х часов ночи, причём раньше я никаких симптомов не ощущал. Это серьезный Звоночек???, как
                говорится "Кто предупрежден, тот вооружен". Позаботьтесь, чтобы Helo был у ваших близких, ведь
                "Береженого бог бережет"!!! Кому-то Helo Жизнь Спасёт!!! Жизнь Продлит!!! А это дороже всех денег,
                и различные возражения типа "стоит дорого" здесь вообще неуместны.
            </p>
        </li>
        <li class="item">
            <div class="avatar">
                <img src="/images/user2.png" alt="" class="avatar">
            </div>
            <h1>Елена Лапина, 46 лет, предприниматель</h1>
            <h2>Москва</h2>
            <p>
                Сетевой маркетинг - это удивительное явление, так как именно в этом бизнесе можно раскрыть весь свой
                потенциал без серьёзных капиталовложений. Этот бизнес подходит абсолютно всем вне зависимости от Вашего
                географического месторасположения, образования и социального статуса. Все, что нужно для того, чтобы
                преуспеть в этом бизнесе - это только Ваше ЖЕЛАНИЕ. А за желанием всегда идут ДЕЙСТВИЯ. И Вы успешны!
            </p>
        </li>
        <li class="item">
            <div class="avatar">
                <img src="/images/user4.png" alt="" class="avatar">
            </div>
            <h1>Сато Хирохиса</h1>
            <h2>Япония</h2>
            <p>
                Это замечательный браслет, я таких в Японии ещё не видел, думаю японцы будут заинтересованы таким
                браслетом! Я с удовольствием ношу его и благодаря программе в телефоне постоянно вижу своё состояние
                здоровья!
            </p>
        </li>

        <li class="item">
            <div class="avatar">
                <img src="/images/user3.png" alt="" class="avatar">
            </div>
            <h1>Ида Сенсеи</h1>
            <h2>Япония</h2>
            <p>
                Мне уже почти 70 лет и моё давление достаточно высокое и болезни часто тревожат, поэтому такой браслет
                как Helo мне настоящий друг и помощник. С ним я в безопасности! Благодаря функции We care моя дочь также
                видит моё состояние здоровья.
            </p>
        </li>
        <li class="item">
            <div class="avatar">
                <img src="/images/user5.png" alt="" class="avatar">
            </div>
            <h1>Елена Мейер, 36 лет, домохозяйка</h1>
            <h2>Одесса</h2>
            <p>
                Очень давно находилась в поиске заработка. И мне чудом попалось на глаза видео о чудо браслете, который
                позволяет видеть состояние здоровья все самых дорогих людей. Меня это очень вдохновило. Я купила и
                быстро получила 5 своих HELO и тут началось. Всем моим знакомым стало дико интересно и меня засыпали
                вопросами. Прошел месяц и я полностью окупила свою покупку и уже начался заработок. Супер!!! Если вы в
                поиске успеха, присоединяйтесь!
            </p>
        </li>
        <li class="item">
            <div class="avatar">
                <img src="/images/user6.png" alt="" class="avatar">
            </div>
            <h1>Александр Паршин, 44 года, рабочий</h1>
            <h2>Сургут</h2>
            <p>
                Отличная компания. Отличные разработки и самое главное что любой может брать и зарабатывать деньги,
                совершая рекомендации друзьям. И понял, что это не сложно. Отличный бизнес с классным продуктом, это то,
                что нужно многим в тяжелое время кризиса. Самое классное, это видеть радостные лица друзей, которые
                благодаря моей информации, получают отличный результат.<br>Я доволен! Спасибо WOR(l)D!
            </p>
        </li>
    </ul>
</div>
<div class="wrapper enroll enroll1">
    <a href="<?= $user['world_login'] ?>" class="button">Стать партнёром Wor(l)d</a>
</div>
<div class="wrapper helo-diff">
    <h1>Сравнение популярных браслетов и HELO</h1>
    <h2>По функционалу БРАСЛЕТ HELO не имеет аналогов во всём мире</h2>
    <img class="helo-diff-img" src="/images/helo_diff.png" alt="">
</div>
<div class="wrapper enroll enroll2">
    <a href="<?= $user['world_login'] ?>" class="button">Стать партнёром Wor(l)d</a>
</div>
<div class="wrapper-fluid stones">
    <div class="wrapper">
        <h1>Полезные камни в браслете HELO</h1>
        <h2>Использование данных минералов имеет доказанное воздействие на организм человека,
            что подтверждено многочисленными исследованиями</h2>
        <ul class="stones-list">
            <li>
                <img class="stone" src="/images/stone-germanium.png" alt="">
                <p>
                    <strong>Германий</strong> является полупроводниковым материалом, доступным в природе. Это природный
                    антиоксидант. Он
                    содержит тебе самые ингредиенты, которые можно найти также в грибах, растениях, таких как женьшень,
                    алое и чеснок.
                </p>
                <a href="#advantage1" class="button">Подробнее</a>
            </li>
            <li>
                <img class="stone" src="/images/stone-himalayan-salt.png" alt="">
                <p>
                    <strong>Гималайская соль</strong> является преимущественно хлоридом натрия и содержит такие
                    же 84 природных минерала и элемента, которые найдены в человеческом теле. Гималайский кристалл
                    повышает уровень энергии, обладает высокоэнергетическими вибрациями, и пользуется большим спросом
                    как универсальный кристалл для повышения уровня самочувствия.
                </p>
                <a href="#advantage2" class="button">Подробнее</a>
            </li>
            <li>
                <img class="stone" src="/images/stone-hematite.png" alt="">
                <p>
                    <strong>Гематит</strong> Гематит улучшает усвоение железа в тонком кишечнике и стимулирует
                    производство красных кровяных клеток. Это способствует насыщению кислородом всех тканей организма и
                    улучшению общего состояния здоровья.
                </p>
                <a href="#advantage3" class="button">Подробнее</a>
            </li>
        </ul>
    </div>
</div>
<div class="wrapper advantages">
    <h1>Презентация HELO и бизнес-возможности</h1>
    <h2>Внимательно посмотрите эту презентацию</h2>
    <div class="video">
        <iframe src="https://www.youtube.com/embed/HFAzQXslhD4?rel=0" frameborder="0"
                allowfullscreen></iframe>
    </div>
</div>
<div class="wrapper enroll enroll3">
    <a href="<?= $user['world_login'] ?>" class="button">Стать партнёром Wor(l)d</a>
</div>
<div class="wrapper change-me">
    <h1>Всего 4 первых партнера и Ваш доход более 50,000 рублей</h1>
    <img class="change-me-img" src="/images/change_me.png" alt="">
</div>
<div class="wrapper business-plan">
    <h1>Всего 4 первых партнера и Ваш доход более 50,000 рублей</h1>
    <h2>Данный план проверен и перевыполнен партнерами Wor(l)d</h2>
    <table>
        <tr style="border-bottom: 2px solid white;">
            <th style="width: 233px">Месяц</th>
            <th style="width: 255px">Всего партнеров</th>
            <th>Ваш доход на пакете Family (руб./мес)</th>
        </tr>
        <tr>
            <td>1</td>
            <td>2</td>
            <td>31.500</td>
        </tr>
        <tr>
            <td>2</td>
            <td>4</td>
            <td>12.600</td>
        </tr>
        <tr>
            <td>3</td>
            <td>8</td>
            <td>25.200</td>
        </tr>
        <tr>
            <td>4</td>
            <td>16</td>
            <td>50.400</td>
        </tr>
        <tr>
            <td>5</td>
            <td>32</td>
            <td>100.800</td>
        </tr>
        <tr>
            <td>6</td>
            <td>64</td>
            <td>201.600</td>
        </tr>
        <tr>
            <td>7</td>
            <td>128</td>
            <td>403.200</td>
        </tr>
        <tr>
            <td>8</td>
            <td>256</td>
            <td>806.400</td>
        </tr>
        <tr>
            <td>9</td>
            <td>512</td>
            <td>1.612.800</td>
        </tr>
        <tr>
            <td>10</td>
            <td>1.024</td>
            <td>3.225.600</td>
        </tr>
        <tr>
            <td>11</td>
            <td>2.048</td>
            <td>6.451.200</td>
        </tr>
        <tr>
            <td>12</td>
            <td>4.096</td>
            <td>12.902.400</td>
        </tr>
    </table>
    <p style="color: #ba090e">ОБЯЗАТЕЛЬНО: 1 партнер подключает 2-х друзей на пакет Family за первый месяц и всё</p>
    <p>ОГРАНИЧЕНИЕ: 200.000$ (12.600.000 руб. за 1 отчетный период (2 недели))</p>
</div>
<div class="wrapper-fluid steps">
    <div class="wrapper">
        <h1>Как начать зарабатывать</h1>
        <h2>Чёткий пошаговый план, который гарантированно приводит к результату</h2>
    </div>
    <div class="wrapper-fluid crumbs">
        <div class="wrapper">
            <ul>
                <li>
                    <div class="icon"><i class="fa fa-comments" aria-hidden="true"></i></div>
                    <h3>Знакомство</h3>
                    <span>Вам надо позвонить мне в WhatsApp или Skype. Мы знакомимся и разбираем все возникшие вопросы.</span>
                </li>
                <li>
                    <div class="icon"><i class="fa fa-pencil" aria-hidden="true"></i></div>
                    <h3>Регистрация</h3>
                    <span>Бесплатно регистрируетесь по моей реферальной ссылке и получаете свой кабинет и интернет-магазин.</span>
                </li>
                <li>
                    <div class="icon"><i class="fa fa-area-chart" aria-hidden="true"></i></div>
                    <h3>Маркетинг</h3>
                    <span>Вместе разбираем маркетинг план и Вы определяетесь с своим желаемым уровнем дохода</span>
                </li>
                <li>
                    <div class="icon"><i class="fa fa-book" aria-hidden="true"></i></div>
                    <h3>Обучение</h3>
                    <span>Вместе проходим стартовый Power Start тренинг и планируем совместные шаги. </span>
                </li>
                <li>
                    <div class="icon"><i class="fa fa-money" aria-hidden="true"></i></div>
                    <h3>Доход</h3>
                    <span>Начинаем действовать по системе и уже через 7 дней Вы увидите свой первый доход.</span>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="wrapper enroll">
    <a href="<?= $user['world_login'] ?>" class="button">Стать партнёром Wor(l)d</a>
</div>
<div class="wrapper-fluid footer">
    <div class="wrapper">
        <footer>
            <a href="/"><img class="logo" src="/images/logo.png" alt=""></a>
            <a href="#develop" class="button">Заказать такой сайт</a>
            <div class="contacts">
                <a href="whatspp://<?= $user['whatsapp'] ?>" class="contact whatsapp"><i class="fa fa-whatsapp"
                                                                             aria-hidden="true"></i><?= $user['whatsapp'] ?></a>
                <a href="skype:<?= $user['skype'] ?>" class="contact skype"><i class="fa fa-skype" aria-hidden="true"></i><?= $user['skype'] ?></a>
            </div>
            <div class="social">
                <?php if ($user['instagram']) { ?><a href="<?= $user['instagram'] ?>"><img src="/images/social-instagram.png" alt=""></a><?php } ?>
                <?php if ($user['vk']) { ?><a href="<?= $user['vk'] ?>"><img src="/images/social-vk.png" alt=""></a><?php } ?>
                <?php if ($user['fb']) { ?><a href="<?= $user['fb'] ?>"><img src="/images/social-fb.png" alt=""></a><?php } ?>
            </div>
        </footer>
    </div>
</div>
<a href="#x" class="overlay" id="advantage2"></a>
<div class="popup">
    <div class="container">
        <p>
            Розовая соль издревне была высоко оценена врачами и знахарями, за ее прекрасные лечебные свойства. Еще Александр
            Македонский приказал вывозить ее слонами по индийскому хребту для царских семей и аристократии того времени.
            Древняя
            индийская медицина - аюрведа, называет эту соль черной (на санскрите: "кала намак"), так как в виде камней она
            имеет
            черный, слегка красноватый цвет.
        </p>

        <h3>Лечебные свойства гималайской соли:</h3>
        <ul>
            <li>- Очищает организм от шлаков</li>
            <li>- Восстанавливает и балансирует водно-солевойобмен</li>
            <li>- Стимулирует аппетит</li>
            <li>- Способствует регенерации клеток и омоложениювсего организма</li>
            <li>- Расслабляет мышечные ткани</li>
            <li>- Уравновешивает психологическое состояние</li>
            <li>- Комплексный терапевтический эффект прииспользовании в ваннах</li>
            <li>- Является источником важных микроэлементов</li>
            <li>- Улучшает циркуляцию крови и лимфы</li>
            <li>- Уменьшает похмельный синдром</li>
            <li>- Помогает от боли в суставах</li>
        </ul>
        <h3>Показания к применению Гималайской соли:</h3>
        <ul>
            <li>- Ослабление иммунной системы – синдромхронической усталости</li>
            <li>- Болезни сердечно-сосудистой системы</li>
            <li>- Болезни мочеполовой системы</li>
            <li>- Болезни опорно-двигательного аппарата(заболевания позвоночника, подагра, ревматизм,артроз, артриты и
                т.д.)
            </li>
            <li>- Эндокринные заболевания (тиреотоксикоз,ожирение, мастопатия, женское бесплодие)</li>
            <li>- Проблемы с метаболизмом</li>
            <li>- Нарушения сна</li>
            <li>- Болезни кожи (герпес, сыпь, псориаз и т.д.)</li>
            <li>- Гипертония</li>
            <li>- Отравления</li>
            <li>- Водянка</li>
            <li>- Астма</li>
        </ul>
    </div>
    <a class="close" title="Закрыть" href="#close"></a>
</div>
<a href="#x" class="overlay" id="advantage1"></a>
<div class="popup">
    <div class="container">
        <h2>Лечебные свойства Германия</h2>
        <p>
            Для медицинских нужд Германий начали применять в Японии.
            Испытания показали, что применение Германия положительно влияет на организм человека. Прорыв наступил в 1987
            году,
            когда доктор Казухико Асау обнаружил, что органический Германий обладает широким спектром биологического
            действия.
        </p>
        <p>
            Среди биологических свойств Германия можно отметить его способности:
        </p>

        <ul>
            <li>- обеспечивает перенос кислорода в тканях организма и улучшает работу мозга</li>
            <li>- повышает иммунитет</li>
            <li>- ускоряет заживление ран</li>
            <li>- проявляет противоопухолевую активность</li>
            <li>- успокаивает нервную систему</li>
            <li>- способствует очищению клеток и тканей от токсинов и ядов</li>
            <li>- улучшает состояние центральной нервной системы и ее функционирование</li>
            <li>- ускоряет восстановление после тяжелой физической нагрузки</li>
            <li>- повышает общую работоспособность человека</li>
            <li>- усиливает защитные реакции всей иммунной системы</li>
            <li>- предупреждает кислородную недостаточность</li>
            <li>- помогает выработке гамма-интерферонов</li>
        </ul>
        <p>
            Взаимодействует Германий с человеческим организмом не только на клеточном уровне.
            При тактильном контакте с Германием (то есть когда есть контакт непосредственно с телом), благодаря
            проникающей
            способности электронов, в организме происходят сложные биохимические процессы, которые так благотворно
            влияют на
            наш
            организм. Значительно возрастает лифмодренажная функция тканей, нормализуется эластичность и тонус сосудов -
            в
            них
            возрастает кровоток, увеличивается диаметр капилляров, что производит сосудорасширяющее, противоотечное и
            иммуностимулирующее действие.
        </p>
        <p>
            Постоянное ношение браслета HELO с Германием способствует:
        </p>
        <ul>
            <li>- целостному оздоровлению кровеносной системы, улучшению свертываемости крови</li>
            <li>- к насыщению клеток кислородом, и, следовательно, их омоложению - усилению микроциркуляции сосудов,
                повышению эластичности тканей - активизации сосудорасширяющего, противоотечного и иммуностимулирующего
                действия
            </li>
        </ul>
    </div>
    <a class="close" title="Закрыть" href="#close"></a>
</div>
<a href="#x" class="overlay" id="advantage3"></a>
<div class="popup">
    <div class="container">
        <h2>Лечебные свойства Гематита</h2>
        <p>
            Магнитные браслеты относятся к магнитной терапии, которая входит в список нетрадиционной медицины. Они обладают
            свойствами улучшения кровообращения, где основное воздействие идет на капиллярную систему, благодаря которым
            организм получает кислород во всех органах. Магнитные вставки носят на запястье, где расположены основные точки,
            отвечающие за пищеварение, нервную и сердечно-сосудистую систему, кровоток и прочие. Среди всевозможных
            металлов,
            широкую популярность обрел гематит.
        </p>

        <p>
            Гематит обладает исключительным свойством – он не вызывает аллергических реакций, поэтому идеально подходит для
            людей страдающими разного рода аллергиями.
        </p>
        <p>
            Гематит издавна применялся при заболеваниях крови, ранах, порезах, травмах и переломах. Основное действие
            гематит
            оказывает на гемоглобин крови и способствует лучшему усвоению кислорода тка­нями и ферментными системами
            организма.
            Ношение кам­ня хорошо влияет на состав самого гемоглобина, позволяет кислороду образовывать связь с этим важным
            белком крови с меньшими для организма энергозатратами и прод­левает срок жизни эритроцитов. Кроме этого, гематит
            сти­мулирует работу красного костного мозга, увеличивая количество и улучшая качество образования красных
            кро­вяных
            телец. Заметно действие камня и на систему свер­тывания крови – все факторы начинают работать в уси­ленном
            режиме,
            что приводит к ускорению заживления порезов и образования защитного тромба на поверхности раны. Считается, что
            гематит позволяет снять местное воспаление в ране за счет стимуляции образования губи­тельных для
            микроорганизмов
            веществ и усиления обме­на кислорода в сосудистой сети на месте травмы. Сказывается положительное влияние
            минерала и при общих сосудистых расстройствах, особенно связанных с пониженным артериальным давлением. В этих
            случаях влияние гематита позволяет тканям получать большее количество питательных веществ, несмотря на
            ослабле­ние
            кровотока. Камень обеспечивает правильное снабже­ние органов кровью и выведение шлаков при суженных сосудах. Но
            необходимо помнить, что ношение гематита может поднимать артериальное давление на 10-15 мм ртутного столба,
            поэтому
            его нельзя постоянно носить при гипертонической болезни. А вот при вегетососудистой дистонии ношение минерала
            чаще
            всего приносит заметное облегчение, стабилизируя артериальное давление и нор­мализуя обмен веществ в тканях.
        </p>
    </div>
    <a class="close" title="Закрыть" href="#close"></a>
</div>
<a href="#x" class="overlay" id="develop"></a>
<div class="popup form">
    <div class="container">
        <h2>Заявка на создание сайта</h2>
        <form id="getSiteForm" action="/feedback.php" method="post">
            <input type="text" name="name" placeholder="Ваше имя">
            <input type="text" name="contact" placeholder="Номер телефона или email">
            <input type="submit" name="ok" value="Отправить заявку">
        </form>
        <ul id="errors"></ul>
    </div>
    <a class="close" title="Закрыть" href="#close"></a>
</div>
</body>
</html>